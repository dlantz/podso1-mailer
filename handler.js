"use strict";
const AWS = require("aws-sdk");
const SES = new AWS.SES();

function sendEmail(data, callback) {
  const emailParams = {
    Source: "daniel@mailup.net", // SES SENDING EMAIL
    ReplyToAddresses: [data.replyTo],
    Destination: {
      ToAddresses: ["daniel@mailup.net", "paul.gilman3@gmail.com"], // SES RECEIVING EMAIL
    },
    Message: {
      Body: {
        Text: {
          Charset: "UTF-8",
          Data: `${data.message}\n\nName: ${data.name}\nEmail: ${data.replyTo}`,
        },
      },
      Subject: {
        Charset: "UTF-8",
        Data: "[podso1.io] Feedback Received",
      },
    },
  };

  SES.sendEmail(emailParams, callback);
}
module.exports.mailer = (event, context, callback) => {
  const data = JSON.parse(event.body);

  sendEmail(data, function (err, data) {
    const response = {
      statusCode: err ? 500 : 200,
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "https://podso1.io",
      },
      body: JSON.stringify({
        message: err ? err.message : data,
      }),
    };

    callback(null, response);
  });
};
